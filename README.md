# Custom Buildpack

Fork of the official [Python buildpack][1] to add a `bin/test` script which runs
Pytest, as explained [here][2]. The default build pack was causing Auto Devops
to fail at the test stage.

[1]: https://github.com/heroku/heroku-buildpack-python.git
[2]: https://gitlab.com/gitlab-org/gitlab-ce/issues/47651